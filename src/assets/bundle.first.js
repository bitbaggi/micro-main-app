
(function () {
    'use strict';
    (function(){if(void 0===window.Reflect||void 0===window.customElements||window.customElements.polyfillWrapFlushCallback)return;const a=HTMLElement;window.HTMLElement={HTMLElement:function HTMLElement(){return Reflect.construct(a,[],this.constructor)}}.HTMLElement,HTMLElement.prototype=a.prototype,HTMLElement.prototype.constructor=HTMLElement,Object.setPrototypeOf(HTMLElement,a);})();
}());
  (function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/elements'), require('@angular/platform-browser'), require('@angular-mfe/shared')) :
    typeof define === 'function' && define.amd ? define('@angular-mfe/first', ['exports', '@angular/core', '@angular/elements', '@angular/platform-browser', '@angular-mfe/shared'], factory) :
    (global = global || self, factory((global.mfe = global.mfe || {}, global.mfe.first = {}), global.ng.core, global.ng.elements, global.ng.platformBrowser, global.mfe.shared));
}(this, (function (exports, core, elements, platformBrowser, shared) { 'use strict';

    var FirstMfe = /** @class */ (function () {
        function FirstMfe(sharedMfe) {
            this.sharedMfe = sharedMfe;
            sharedMfe.registerMfe('MFE-ONE' + Math.floor(Math.random() * 10), FirstMfe);
            sharedMfe.initInsights({ instrumentationKey: "XXX" });
            sharedMfe.logEvent({ trackingEvent: "TEST", properties: {} });
        }
        FirstMfe.ɵfac = function FirstMfe_Factory(t) { return new (t || FirstMfe)(core["ɵɵdirectiveInject"](shared.SharedMfe)); };
        FirstMfe.ɵcmp = core["ɵɵdefineComponent"]({ type: FirstMfe, selectors: [["ng-component"]], decls: 2, vars: 0, template: function FirstMfe_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "p");
                core["ɵɵtext"](1, "First Angular Micro Frontend");
                core["ɵɵelementEnd"]();
            } }, encapsulation: 2 });
        return FirstMfe;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FirstMfe, [{
            type: core.Component,
            args: [{
                    template: "<p>First Angular Micro Frontend</p>"
                }]
        }], function () { return [{ type: shared.SharedMfe }]; }, null); })();
    var FirstMfeModule = /** @class */ (function () {
        function FirstMfeModule(injector) {
            var customElement = elements.createCustomElement(FirstMfe, { injector: injector });
            customElements.define('mfe-one', customElement);
        }
        FirstMfeModule.prototype.ngDoBootstrap = function () { };
        FirstMfeModule.ɵmod = core["ɵɵdefineNgModule"]({ type: FirstMfeModule });
        FirstMfeModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function FirstMfeModule_Factory(t) { return new (t || FirstMfeModule)(core["ɵɵinject"](core.Injector)); }, imports: [[platformBrowser.BrowserModule]] });
        return FirstMfeModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](FirstMfeModule, { declarations: [FirstMfe], imports: [platformBrowser.BrowserModule] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](FirstMfeModule, [{
            type: core.NgModule,
            args: [{ declarations: [FirstMfe], imports: [platformBrowser.BrowserModule] }]
        }], function () { return [{ type: core.Injector }]; }, null); })();
    // If there is already a platform, reuse it, otherwise create a new one
    (core.getPlatform() || platformBrowser.platformBrowser()).bootstrapModule(FirstMfeModule).catch(function (err) { return console.log(err); });

    exports.FirstMfe = FirstMfe;
    exports.FirstMfeModule = FirstMfeModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=angular-mfe-first.umd.js.map

