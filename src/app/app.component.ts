import { Component } from '@angular/core';
import {SharedMfe} from '@angular-mfe/shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  constructor(sharedMfe: SharedMfe){
    sharedMfe.registerMfe('main', null);
  }
}
